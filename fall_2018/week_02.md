## Last Week's Accomplishments

* got instruction counting working in c
* decided to write rust wrappers for syscalls such as perf_event_open

## This Week's Plan

* decide on a way to do instruction counting and decide between performance and or portability
* develop more proof of concepts to see how they work

## Anything Blocking?

* ptraces documentation is lacking
* does performance matter that much

## Notes

* I may start a blog to document the research process
