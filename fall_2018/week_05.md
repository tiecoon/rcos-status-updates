## Last Week's Accomplishments

* setup bindgen for support with c bitfields
* solved issues with forking in threads using rust processes
* resolved a header issue

## This Week's Plan

* get it working on the csaw binary wyvern

## Anything Blocking?

* make sure bindgen is properly handling the bitfields
* lacking test cases still

## Notes

* blog themes are a tough choice
