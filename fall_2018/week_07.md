## Last Week's Accomplishments

* implemented the input generators and brute force function for stdin
* it automatically solves wyvern now
* setup event hooks on the generators for a potential tui

## This Week's Plan

* try to get argv generators working
* maybe setup a tui if i get bored

## Anything Blocking?

* no

## Notes

* finally picked a theme for the blog
* using same postts from rpisec for now will add posts as i make them
* http://www.bierysbargainbarn.com/
