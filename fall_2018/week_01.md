## Last Week's Accomplishments

* setup ptrace and fork boilerplate in rust
* setup initial repo and configure rust
* setup vim for rust

## This Week's Plan

* be able to get a working instruction count
* make sure everyone else is setup with atleast formatting

## Anything Blocking?

* ptraces documentation is lacking
* there appears to be an already attached process to the ptracee fork

## Notes
