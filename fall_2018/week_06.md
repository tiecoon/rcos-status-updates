## Last Week's Accomplishments

* got basic outlier calculations done
* figured out with the group the class structure and how we will implement it in the end

## This Week's Plan

* get it working on wyvern
* try to begin writing test cases

## Anything Blocking?

* test cases on cloud ci will be hard to test as im dont know how well they support perf if at all.

## Notes

* Blogs need a good theme before content
* ricing a blog is distracting and more atractive then actual work
